import unittest

# Erstellen Sie für die folgenden Methoden Tests.
# Nutzen Sie die Klasse TestBeispieleTestase für die erstellten Tests.
# Die Tests können über die Konsole mit python <Name_der_Datei>.py ausgeführt werden.
# Viel Spaß :-)!



def hello_world():
    return

def erzeuge_list_mit_vorgegener_laenge(laenge):
    return

def entferne_alle_leerzeichen_aus_einem_string(leerzeichen_string):
    return


def erzeuge_eine_sortierte_list_ohne_duplikate(liste):
    return


class TestBeispieleTestCase(unittest.TestCase):
    def test_hello(self):
        self.assertEqual(hello_world(), 'hello world')

    def test_erzeuge_list_mit_vorgegener_laenge(self):
        ergebnis = len(erzeuge_list_mit_vorgegener_laenge(10))
        self.assertEqual(ergebnis, 10)

    def test_entferne_alle_leerzeichen_aus_einem_string(self):
        ergebnis = entferne_alle_leerzeichen_aus_einem_string("hallo  w   e lt")
        self.assertEqual(ergebnis, "hallowelt")

    def test_erzeuge_eine_sortierte_list_ohne_duplikate(self):
        liste = [1,2,2,4,-1,0,0]
        ergebnis = erzeuge_eine_sortierte_list_ohne_duplikate(liste)
        self.assertEqual(ergebnis,[-1,0,1,2,4])

# Verbosity=2 sorgt dafür, dass die Ausgaben "wortreichener" sind.
# Wenn Sie den Level auf 1 ändern, erkennen Sie den Unterschied.
if __name__=='__main__':
    unittest.main(verbosity=2)
